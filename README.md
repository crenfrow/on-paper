# on paper, a Zola theme

This theme was written for and by myself to use with a static site generator called [Zola](https://getzola.org) using [TailwindCSS](https://tailwindcss.com) for styling. I tried to keep things simple and designed with responsiveness in mind.

This is very much a work-in-progress, but you may use and adapt at your own discretion.

## Preview

**Desktop**

![Desktop format](./assets/desktop-preview.png){width=75%}

**Mobile**

![Mobile format](./assets/mobile-preview.png){width=25%}

## Usage

**A note before you begin:** This theme a little different than a lot of Zola themes out there. It is intended that no pages aside from index and 404 will render (this may change in the future) despite making use of multiple sections and pages in the `content` directory. Take a look at the example content and see if it works for you before continuing.

1. Follow the Zola documentation for [installing and using themes](https://www.getzola.org/documentation/themes/installing-and-using-themes/)
2. Override the example config's `[extra]` fields
3. **Suggestion:** Copy the theme's content directory to use as a template
  - `cp -r ./themes/on-paper/content ./content`

## Development Usage

If you're extending or contributing to this theme, here's how to get started.

### Dependencies

#### With `nix`
1. Run `nix develop` to create an ephemeral shell with dependencies

#### Without `nix`
1. [Install `zola`](https://www.getzola.org/documentation/getting-started/installation/) 
2. [Install `tailwindcss`](https://tailwindcss.com/docs/installation)

### Workflow
1. In one terminal run `tailwindcss -i styles/input.css -o static/styles/styles.css --watch`
2. In another run `zola serve`
3. Make changes and ensure that rebuilds are triggering as expected

## TODO

- [x] Write documentation
  - [x] Requirements
  - [x] Usage
- [ ] Do some clean-up, kill some zombie macros, etc.
- [ ] Replace horrible usage of shortcodes
- [ ] Fix anchor tags not being highlighted in MD content
