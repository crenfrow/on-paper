{
  description = "A Nix-flake-based development environment for Zola templates and themes";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs";
  };

  outputs =
    { self
    , flake-utils
    , nixpkgs
    }:

    flake-utils.lib.eachDefaultSystem (system:
    let
        overlays = [
            (self: super: rec {
                nodejs = super.nodejs-18_x;
                tailwindcss = super.nodePackages.tailwindcss;
                yarn = (super.yarn.override {inherit nodejs; });
            })
        ];
        pkgs = import nixpkgs { inherit overlays system; };
    in
    {
      devShells.default = pkgs.mkShell {
        nativeBuildInputs = with pkgs; [
            nodejs
            tailwindcss
            yarn
            zola
        ];

        shellHook = ''
            echo nodejs ${pkgs.nodejs.version} 
            echo yarn ${pkgs.yarn.version}
            echo tailwindcss ${pkgs.tailwindcss.version}
            ${pkgs.zola}/bin/zola --version
        '';
      };
    });
}