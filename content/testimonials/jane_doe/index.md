+++
weight = 0
[extra]
praise = "They always ensure their deliverables are shipped on time and their attention to detail shows in all of their work."
author = "Jane Doe"
affiliation = "Client"
+++
