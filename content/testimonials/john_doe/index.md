+++
weight = 1
[extra]
praise = "Sam is a great person to pair[-program] with, I would relish the opportunity to work with them on the same team!"
author = "John Doe"
affiliation = "Peer at The Program"
photo = "./johndoe.png"
+++
