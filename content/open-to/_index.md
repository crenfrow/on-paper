+++
sort_by = "weight"
render = false
+++

I am looking for traditional software developer roles at companies that value positive work culture and lifelong learning. Let's [connect](/#let-s-get-in-touch")!
