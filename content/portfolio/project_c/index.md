+++
title = "Project C"
description = "Yet an additional really cool project that demonstrates my capabilities in another area."
weight = 2
[extra]
completed = true
start = 2023-01-29
end = 2023-02-03
link = "#"
hero_img = "./hero.jpg"
+++

A few paragraphs describing my role in the project and what I did that makes my work stand out.