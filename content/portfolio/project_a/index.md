+++
title = "Project A"
description = "A really cool project that demonstrates my capabilities in a certain area."
weight = 0
[extra]
completed = false
start = 2023-02-01
# end = 
url = "#"
hero_img = "./hero.jpg"
+++

A few paragraphs describing my **role** in *The Project* and what I did that makes [my work](#) stand out.

<!-- more -->
